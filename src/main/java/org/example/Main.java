package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("You have to run this program with 3 params: dbUrl, dbUsername, dbPassword!");
            return;
        }
        try {
            final Connection connection = getConnection(args[0], args[1], args[2]);
            System.out.println("connection -> " + connection);
            UserDAO userDAO = new UserDAO(connection);
            userDAO.listAllUsers();
            userDAO.updateUsername("id1", "usernameFromMainJava");
            userDAO.listAllUsers();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection(String dbUrl, String username, String password) throws SQLException {
        return DriverManager.getConnection(dbUrl, username, password);
    }
}
