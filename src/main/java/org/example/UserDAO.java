package org.example;

import org.example.entities.UserEntity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDAO {

    private final Connection connection;

    public UserDAO(Connection connection) {
        this.connection = connection;
    }

    public void listAllUsers() {
        try (Statement stmt = this.connection.createStatement()) {
            ResultSet rs = stmt.executeQuery("select * from user");

            int i = 0;
            while (rs.next()) {
                String id = rs.getString("id");
                String username = rs.getString("username");
                UserEntity userEntity = new UserEntity(id, username);
                System.out.println("Row " + i++ + ":" + userEntity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUsername(String id, String newUsername) {
        try (Statement stmt = this.connection.createStatement()) {
            stmt.executeUpdate("update user set username=\"" + newUsername + "\" where id=\"" + id + "\"");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
