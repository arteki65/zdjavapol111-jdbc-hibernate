package org.example.entities;

// user table
public class UserEntity {
    private final String id;

    private final String username;

    public UserEntity(String id, String username) {
        this.id = id;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "UserEntity{" + "id='" + id + '\'' + ", username='" + username + '\'' + '}';
    }
}
